<?php   # Copyright 2015 ShilGen.ru geniusshil@gmail.com 
if ((isset($_SERVER['QUERY_STRING'])) and ($_SERVER['REMOTE_ADDR'] == '127.0.0.1') ){
	define('DEBUG', 1);
	error_reporting(E_ALL);
	ini_set('display_errors', true);
	ini_set('html_errors', true);
} else {
	define('DEBUG', 0);
}

if (!defined('_BR_')) define('_BR_',chr(13).chr(10));
function __autoload( $className ) {
	$className = str_replace( "..", "", $className );
	require_once( "core/$className.php" );
}
	
function Get_url() {
	$currentUrl ='';
	if (isset($_SERVER['SERVER_PROTOCOL'])) $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http' : 'https';
	if (isset($_SERVER['HTTP_HOST'])) {
		$host = $_SERVER['HTTP_HOST'];
		$currentUrl = $protocol . '://' . $host;
	}
	if (isset($_SERVER['SCRIPT_NAME'])) {
		$script   = $_SERVER['SCRIPT_NAME'];
		$currentUrl =$currentUrl . $script ;
	}
	if (isset($_SERVER['QUERY_STRING'])) {
		$params   = $_SERVER['QUERY_STRING'];
		if (empty(!$params)){$currentUrl .=  '?' . $params;}
	}
	return $currentUrl;
}
?>
