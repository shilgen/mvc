<?php   # Copyright 2015 ShilGen.ru geniusshil@gmail.com 
class View{
	private $model;
	private $controller;
	public function __construct($controller,$model) {
		$this->controller = $controller;
		$this->model = $model;
	}
	
	public function output() {
		return "<p><a href=\"index.php?action=clicked\">" . $this->model->string . "</a></p>";
	}
	private $file_tpl;
	private $data;

		
	public function Get_html($file_tpl, $data) {
		$html_from_tpl = file_get_contents($file_tpl) ;
		if (is_array ($data)) foreach ($data as $k=>$v) $html_from_tpl = str_replace("{".$k."}", $v, $html_from_tpl);
		echo $html_from_tpl;
	}
}

class V_get_head  extends  View {
	public function get_head_data($data_value) {
		$k = array('title_page', 'description', 'keywords');
		$page_head = array_combine($k, $data_value);
		$this ->Get_html( "tpl/html_head.tpl",$page_head);
		}
	}
?>
